import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Input, Flex, Button } from "@chakra-ui/react";
import { fetchCountries } from "../redux/countriesAction";
import { fetchHarbours } from "../redux/harboursAction";
import { fetchDevice } from "../redux/devicesAction";
import { fetchPrice } from "../redux/pricesAction";
const Form = () => {
  const dispatch = useDispatch();
  const selectedCountry =
    useSelector((state) => state.countries.selectedCountry) || [];
  const selectedHarbour =
    useSelector((state) => state.harbours.selectedHarbour) || [];
  const selectedDevice =
    useSelector((state) => state.devices.selectedDevice) || [];
  const selectedPrice =
    useSelector((state) => state.prices.selectedPrice) || [];
  const selectedTarifBM =
    useSelector((state) => state.tarifBM.selectedTarifBM) || [];

  const [selectNegara, setSelectNegara] = useState("");
  const [inputNegara, setInputNegara] = useState("");
  const [inputPelabuhan, setInputPelabuhan] = useState("");
  const [inputBarang, setInputBarang] = useState("");
  const [inputHarga, setInputHarga] = useState("");
  const rupiah = (number) => {
    if (number === 0) {
      return "FREE";
    } else {
      return new Intl.NumberFormat("id-ID", {
        style: "currency",
        currency: "IDR",
      }).format(number);
    }
  };
  const handleNegaraChange = (e) => {
    const value = e.target.value;
    setInputNegara(value);
    if (value.length >= 3) {
      dispatch(fetchCountries(value));
    }
  };
  const handlePelabuhanChange = (e) => {
    const value = e.target.value;
    setInputPelabuhan(value);
  };
  const handleHargaChange = (e) => {
    const value = e.target.value;
    setInputHarga(value);
  };

  const handleBarangChange = (e) => {
    const value = e.target.value;
    setInputBarang(value);
  };

  useEffect(() => {
    if (inputPelabuhan.length >= 3) {
      dispatch(fetchHarbours(selectNegara, inputPelabuhan));
    } else {
      dispatch({ type: "SET_SELECTED_HARBOUR", payload: [] });
    }
  }, [inputNegara, inputPelabuhan, dispatch]);

  useEffect(() => {
    if (inputBarang) {
      dispatch(fetchDevice(inputBarang));
    } else {
      dispatch({ type: "SET_SELECTED_DEVICE", payload: [] });
    }
  }, [inputBarang]);

  useEffect(() => {
    if (inputBarang) {
      dispatch(fetchPrice(inputBarang, inputHarga));
    } else {
      dispatch({ type: "SET_SELECTED_PRICE", payload: [] });
      dispatch({ type: "SET_SELECTED_TARIF_BM", payload: [] });
    }
  }, [inputBarang, inputHarga]);

  return (
    <Flex
      flexDir="column"
      justifyContent="left"
      textAlign="left"
      gap="50px"
      w="1000px"
      border="3px solid black"
      p="50px"
      m="50px"
    >
      <Flex flexDir="row">
        <Flex w="250px" fontWeight="bold">
          Negara
        </Flex>
        <Flex flexDir="row" gap="15px">
          <Input
            type="text"
            value={inputNegara}
            onChange={handleNegaraChange}
            border="3px solid black"
            px="25px"
            py="10px"
            w="200px"
            h="25px"
          />
          {inputNegara && (
            <Flex
              flexDir="column"
              h="100px"
              w="250px"
              py="10px"
              px="25px"
              overflowX={"auto"}
              overflowY={"auto"}
              sx={{
                "::-webkit-scrollbar": {
                  height: "0.3em",
                  width: "0.3em",
                  backgroundColor: "none",
                  borderRadius: "10px",
                },
                "::-webkit-scrollbar-thumb": {
                  // backgroundColor: '#181D31',
                  backgroundColor: "gray.200",
                  borderRadius: "10px",
                },
                "::-webkit-scrollbar-thumb:hover": {
                  backgroundColor: "#555555",
                  borderRadius: "10px",
                },
              }}
            >
              {selectedCountry
                ?.filter((item) =>
                  item.ur_negara
                    .toLowerCase()
                    .includes(inputNegara.toLowerCase())
                )
                .map((item) => (
                  <Button
                    value={item.ur_negara}
                    key={item.id}
                    px="10px"
                    justifyContent={"left"}
                    w="210px"
                    h="25px"
                    border={"white"}
                    backgroundColor="white"
                    variant="ghost"
                    colorScheme="teal"
                    onClick={(e) => {
                      setInputNegara(e.target.value);
                      setSelectNegara(item.kd_negara);
                      dispatch({
                        type: "SET_SELECTED_COUNTRY",
                        payload: [],
                      });
                    }}
                    _hover={{ background: "teal", color: "white" }}
                  >
                    {item.ur_negara}
                  </Button>
                ))}
            </Flex>
          )}
        </Flex>
      </Flex>
      <Flex flexDir="row">
        <Flex w="250px" fontWeight="bold">
          Pelabuhan
        </Flex>
        <Flex flexDir="row" gap="15px">
          <Input
            type="text"
            value={inputPelabuhan}
            onChange={handlePelabuhanChange}
            border="3px solid black"
            px="25px"
            py="10px"
            w="200px"
            h="25px"
          />
          {inputPelabuhan && (
            <Flex
              flexDir="column"
              h="100px"
              w="250px"
              py="10px"
              px="25px"
              overflowX={"auto"}
              overflowY={"auto"}
              sx={{
                "::-webkit-scrollbar": {
                  height: "0.3em",
                  width: "0.3em",
                  backgroundColor: "none",
                  borderRadius: "10px",
                },
                "::-webkit-scrollbar-thumb": {
                  // backgroundColor: '#181D31',
                  backgroundColor: "gray.200",
                  borderRadius: "10px",
                },
                "::-webkit-scrollbar-thumb:hover": {
                  backgroundColor: "#555555",
                  borderRadius: "10px",
                },
              }}
            >
              {selectedHarbour
                ?.filter((item) =>
                  item.ur_pelabuhan
                    .toLowerCase()
                    .includes(inputPelabuhan.toLowerCase())
                )
                .map((item) => (
                  <Button
                    key={item.id}
                    value={item.ur_pelabuhan.toUpperCase()}
                    px="10px"
                    justifyContent={"left"}
                    w="210px"
                    h="25px"
                    border={"white"}
                    backgroundColor="white"
                    variant="ghost"
                    colorScheme="teal"
                    _hover={{ background: "teal", color: "white" }}
                    onClick={(e) => {
                      setInputPelabuhan(e.target.value);
                      dispatch({
                        type: "SET_SELECTED_HARBOUR",
                        payload: [],
                      });
                    }}
                  >
                    {item.ur_pelabuhan.toUpperCase()}
                  </Button>
                ))}
            </Flex>
          )}
        </Flex>
      </Flex>
      <Flex flexDir="row">
        <Flex w="250px" fontWeight="bold">
          Barang
        </Flex>

        <Input
          type="text"
          value={inputBarang}
          onChange={handleBarangChange}
          border="3px solid black"
          px="25px"
          py="10px"
          w="200px"
          h="25px"
        />
      </Flex>
      <Flex flexDir="row">
        <Flex w="250px" fontWeight="bold">
          Deskripsi
        </Flex>
        <Flex border="3px solid black" px="25px" py="10px" w="200px" h="150px">
          {selectedDevice && (
            <Flex>
              {selectedDevice.sub_header} {selectedDevice.uraian_id}
            </Flex>
          )}
        </Flex>
      </Flex>
      <Flex>
        <Flex w="250px" fontWeight="bold">
          Harga
        </Flex>
        <Input
          type="number"
          value={inputHarga}
          onChange={handleHargaChange}
          border="3px solid black"
          px="25px"
          py="10px"
          w="200px"
          h="25px"
        />
      </Flex>
      <Flex>
        <Flex w="250px" fontWeight="bold">
          Tarif Bea Masuk (%)
        </Flex>

        <Flex px="25px" py="10px" w="200px" h="25px" border="3px solid black">
          {selectedTarifBM}
        </Flex>
      </Flex>
      <Flex>
        <Flex w="250px" fontWeight="bold">
          Total Tarif
        </Flex>
        <Flex px="25px" py="10px" w="200px" h="25px" border="3px solid black">
          {selectedPrice && <Flex>{selectedPrice}</Flex>}
        </Flex>
      </Flex>
    </Flex>
  );
};

export default Form;
