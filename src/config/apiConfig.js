export const API_URLS = {
  getCountries: "https://insw-dev.ilcs.co.id/my/n/negara",
  getHarbours: "https://insw-dev.ilcs.co.id/my/n/pelabuhan",
  getDevices: "https://insw-dev.ilcs.co.id/my/n/barang",
  getPrices: "https://insw-dev.ilcs.co.id/my/n/tarif",
};
