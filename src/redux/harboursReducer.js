import * as types from "./types";

const initialState = {
  selectedHarbour: null,
};

const harboursReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_SELECTED_HARBOUR:
      return { ...state, selectedHarbour: action.payload };
    default:
      return state;
  }
};

export default harboursReducer;
