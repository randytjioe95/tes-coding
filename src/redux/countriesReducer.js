import * as types from "./types";

const initialState = {
  selectedCountry: null,
};

const countriesReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_SELECTED_COUNTRY:
      return { ...state, selectedCountry: action.payload };
    default:
      return state;
  }
};

export default countriesReducer;
