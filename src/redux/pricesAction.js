import axios from "axios";
import * as types from "./types";
import { API_URLS } from "../config/apiConfig";

const rupiah = (number) => {
  if (number === 0) {
    return "FREE";
  } else {
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
    }).format(number);
  }
};
export const fetchPrice = (hsCode, inputHarga) => {
  return (dispatch) => {
    axios
      .get(`${API_URLS.getPrices}?hs_code=${hsCode}`)
      .then((response) => {
        const selectedTarifBM = response.data.data[0].bm;

        if (selectedTarifBM && inputHarga) {
          const hasil =
            (parseFloat(inputHarga) * parseFloat(selectedTarifBM)) / 100;
          const selectedPrice = rupiah(hasil); // Mengatur angka desimal

          dispatch({
            type: types.SET_SELECTED_PRICE,
            payload: selectedPrice,
          });
          dispatch({
            type: types.SET_SELECTED_TARIF_BM,
            payload: selectedTarifBM,
          });
        } else {
          dispatch(
            {
              type: types.SET_SELECTED_PRICE,
              payload: "",
            },
            { type: types.SET_SELECTED_TARIF_BM, payload: "" }
          );
        }
      })
      .catch((error) => {
        console.error("Error fetching Price:", error);
      });
  };
};
