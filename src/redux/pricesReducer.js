import * as types from "./types";

const initialState = {
  selectedPrice: null,
};

const pricesReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_SELECTED_PRICE:
      return { ...state, selectedPrice: action.payload };

    default:
      return state;
  }
};

export default pricesReducer;
