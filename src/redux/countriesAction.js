import axios from "axios";
import * as types from "./types";
import { API_URLS } from "../config/apiConfig";
let countries = null;
export const setSelectedCountry = (selectedCountry) => {
  return {
    type: types.SET_SELECTED_COUNTRY,
    payload: selectedCountry,
  };
};

export const fetchCountries = (selectedCountry) => {
  return (dispatch) => {
    axios
      .get(`${API_URLS.getCountries}?ur_negara=${selectedCountry}`)
      .then((response) => {
        const selectedCountry = response.data.data;
        dispatch(setSelectedCountry(selectedCountry));
        console.log(selectedCountry);
      })
      .catch((error) => {
        console.error("Error fetching countries:", error);
      });
  };
};
