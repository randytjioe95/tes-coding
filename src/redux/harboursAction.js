import axios from "axios";
import * as types from "./types";
import { API_URLS } from "../config/apiConfig";

export const setSelectedHarbour = (selectedHarbour) => {
  return {
    type: types.SET_SELECTED_HARBOUR,
    payload: selectedHarbour,
  };
};

export const fetchHarbours = (selectedCountry, jur) => {
  return (dispatch) => {
    axios
      .get(
        `${API_URLS.getHarbours}?kd_negara=${selectedCountry}&ur_pelabuhan=${jur}`
      )
      .then((response) => {
        const selectedHarbour = response.data.data;
        dispatch(setSelectedHarbour(selectedHarbour));
      })
      .catch((error) => {
        console.error("Error fetching pelabuhan:", error);
      });
  };
};
