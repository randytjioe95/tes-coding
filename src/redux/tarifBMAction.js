import axios from "axios";
import * as types from "./types";
import { API_URLS } from "../config/apiConfig";

export const fetchPrice = (hsCode, inputHarga) => {
  return (dispatch) => {
    axios
      .get(`${API_URLS.getPrices}?hs_code=${hsCode}`)
      .then((response) => {
        const selectedTarifBM = response.data.data[0].bm;
        dispatch({
          type: types.SET_SELECTED_TARIF_BM,
          payload: selectedTarifBM,
        });
      })
      .catch((error) => {
        console.error("Error fetching Price:", error);
      });
  };
};
