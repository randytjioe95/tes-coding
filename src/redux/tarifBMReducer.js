import * as types from "./types";

const initialState = {
  tarifBM: null,
};

const tarifBMReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_SELECTED_TARIF_BM:
      return { ...state, selectedTarifBM: action.payload };
    default:
      return state;
  }
};

export default tarifBMReducer;
