import axios from "axios";
import * as types from "./types";
import { API_URLS } from "../config/apiConfig";

export const setSelectedDevice = (selectedDevice) => {
  return {
    type: types.SET_SELECTED_DEVICE,
    payload: selectedDevice,
  };
};

export const fetchDevice = (hsCode) => {
  return (dispatch) => {
    axios
      .get(`${API_URLS.getDevices}?hs_code=${hsCode}`)
      .then((response) => {
        const selectedDevice = response.data.data[0];
        dispatch(setSelectedDevice(selectedDevice));
      })
      .catch((error) => {
        console.error("Error fetching Device:", error);
      });
  };
};
