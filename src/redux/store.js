import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import countriesReducer from "./countriesReducer";
import harboursReducer from "./harboursReducer";
import devicesReducer from "./devicesReducer";
import pricesReducer from "./pricesReducer";
import tarifBMReducer from "./tarifBMReducer";
const rootReducer = combineReducers({
  countries: countriesReducer,
  harbours: harboursReducer,
  devices: devicesReducer,
  prices: pricesReducer,
  tarifBM: tarifBMReducer,
});

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;
