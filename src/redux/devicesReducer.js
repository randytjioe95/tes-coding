import * as types from "./types";

const initialState = {
  selectedDevice: null,
};

const devicesReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_SELECTED_DEVICE:
      return { ...state, selectedDevice: action.payload };
    default:
      return state;
  }
};

export default devicesReducer;
