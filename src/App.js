import "./App.css";
import React from "react";
import { Provider } from "react-redux";
import store from "./redux/store";
import Form from "./components/form";
function App() {
  return (
    <>
      <Provider store={store}>
        <div className="App">
          <Form />
        </div>
      </Provider>
    </>
  );
}

export default App;
